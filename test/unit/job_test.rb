require 'test_helper'

class JobTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  setup do
    @job =Job.new
  end

  test "a new job has a status of PROPOSED" do
    @job = Job.new
    assert_equal Job::PROPOSED, @job.status
  end

  test "a job must have an accepted price before being queued" do
    #skip "Comment me out to run this test"
    assert_equal @job.accept(), false, "must have price for transition"
  end

  test "a job with an accepted price can be queued" do
    #skip "Comment me out to run this test"
    @job.price = 1.00
    assert @job.accept()
  end

  test "a job that has been accepted should be queued" do
    #skip "Comment me out to run this test"
    @job.price = 1.00
    @job.accept()
    assert_equal Job::QUEUED, @job.status
  end

end

