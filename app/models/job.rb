class Job < ActiveRecord::Base
  attr_accessible :price, :status
  before_save :set_defaults
end

#first test
PROPOSED = 0

#second test
private
def set_defaults
  self.status = 0
  self.price = 0


#third test
def accept
  if @self.price == 0
    return false
  else
    self.status = 1
    return true
  end
end

#fourth test. Just because its a constant, doesn't mean it has to be one or zero.
def accept
  if self.price = 0
    return false
  else
    self.status = 1
    return true
  end
end

  def PROPOSED
    self.status = 0
  end

  def QUEUED
    self.status = 1
  end

  def PRODUCTION
    self.status = 2
  end

  def TESTING
    self.status = 3
  end

  def CLOSED
    self.status = 4
  end

  def CANCELLED
    self.status = 5
  end

end